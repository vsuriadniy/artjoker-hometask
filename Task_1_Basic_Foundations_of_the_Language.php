<?php

// Написать функцию которая по параметрам принимает число из десятичной системы счисления и преобразовывает в двоичную.
function decimalBin($decimal)
{
    $binary = '';

    while ($decimal >= 1) { 
        $binary = $decimal % 2 . $binary; 
        $decimal /= 2; 
    }

    return $binary;
}

// С помощью рекурсии :
function decimalBin($decimal)
{
    $binary = '';

    if ($decimal < 1) {
        return 0;
    }

    return $binary = $decimal % 2 + 10 * decimalBin($decimal / 2);
}

// Написать функцию которая по параметрам принимает число из двоичной системы счисления и преобразовывает в десятичную.
function binaryDec($binary)
{
    $decimal = 0;
    $base = 1;

    while ($binary) {
        $lastDigit = $binary % 10;
        $binary = $binary / 10;
        $decimal += $lastDigit * $base;
        $base = $base * 2;
    }

    return $decimal;
}

// С помощью рекурсии :
function binaryDec(int $binary)
{
    $decimal = '';

    if($binary < 1){
        return 0;
    }

    return $decimal = $binary % 10 + 2 * binaryDec($binary / 10);
}

// Написать функцию которая выводит первые N чисел фибоначчи
function fibonacciNumbers($number)
{
    if ($number === 0) {
        return 0;
    }

    if ($number === 1 || $number === 2) {
        return 1;
    }

    $fibonacci1 = 0;
    $fibonacci2 = 1;
    $counter = 0;
    $countFibonacci = [$fibonacci1];
    while ($counter < $number - 1) {
        $fibonacciSum = $fibonacci1 + $fibonacci2;
        $fibonacci1 = $fibonacci2;
        $fibonacci2 = $fibonacciSum;
        $countFibonacci[] = $fibonacci1;
        $counter++;
    }
    
    return $countFibonacci;
}

// Через рекурсию :
function fibonacciNumbers($number)
{
    $fibonacciNum = '';

    if ($number <= 1) {
        return 0;
    } 

    if ($number === 2) {
        return 1;
    }

    return $fibonacciNum .= fibonacciNumbers($number - 1) + fibonacciNumbers($number - 2);
}

// Написать функцию, возведения числа N в степень M
function power($number, $degree) 
{
    $result = 1;

    for ($i = 1; $i <= $degree; $i++){
        $result *= $number;
    }

    return $result;
}

// Через рекурсию :
function power($number, $degree)
{
    if ($degree === 1){
        return $number;
    }

    if ($degree !== 1){
        return $number * power($number, $degree - 1); 
    }
}

// Написать функцию которая вычисляет входит ли IP-адрес в диапазон указанных IP-адресов. Вычислить для версии ipv4.
function ipRange($startIp, $endIp, $ip) 
{
    $startIp = explode('.', $startIp);
    $endIp = explode('.', $endIp);
    $ip = explode('.', $ip);   
    $ipArray = [$startIp, $endIp, $ip];

    foreach ($ipArray as $ipArr) {
        if (count($ipArr) !== 4) {
            return false;    
        } else {
            foreach ($ipArr as $number) {
                if (!($number >= 0 && $number <= 255 && is_numeric($number))) {
                    return false;   
                }  
            }
        }
    }

    for ($i = 0; $i < count($ip); $i++) { 
        if(!($ip[$i] >= $startIp[$i] && $ip[$i] <= $endIp[$i])) {
            return false;
        }  
    }

    return true;
}

// Для одномерного массива : подсчитать процентное соотношение положительных/отрицательных/нулевых/простых чисел
// 1.1 процентное соотношение положительных чисел :
function isPositiveNum($value)
{
    return $value > 0;
}

// 1.2 процентное соотношение отрицательных чисел :
function isNegativeNum($value)
{
    return $value < 0;
}

// 1.3 процентное соотношение нулевых чисел :
function isZeroNum($value)
{
    return $value === 0;
}

// 1.4 процентное соотношение простых чисел :
function isPrimeNum($value)
{
    if ($value <= 1) {
        return false;
    }

    for ($i = 2; $i < $value; $i++) {
        if ($value % $i === 0) {
            return false;
        }
    }

    return true;
}

// Через рекурсию :
function isPrimeNum($value, $divider = 0) 
{
    if ($divider === 0) {
        $divider = $value - 1;
    }

    if ($value <= 1) {
        return false;
    }

    while ($divider >= 2) {
        if ($value % $divider === 0) {
            return false;
        }

        return isPrimeNum($value, $divider - 1);
    }

    return true;
}

function getPercentage($array, $callback)
{  
    $count = count($array);
    $percent = 0;
    $counter = 0;

    foreach($array as $value){
        if($callback($value)){
            $counter++; 
        } 
    }

    return $counter * 100 / $count;
}

// 1.5 Отсортировать элементы по возрастанию/убыванию:
// по возрастанию:
function sortAsc($arrayValue1, $arrayValue2)
{
    return $arrayValue1 < $arrayValue2;
}

function sortDesc($arrayValue1, $arrayValue2)
{
    return $arrayValue1 > $arrayValue2;
}

function sortVal($array, $callback)
{
    $count = count($array);
    $result = 0;

    for ($i = $count - 1; $i > 0; $i--) {
        for ($j = 0; $j < $i; $j++) {
            if ($callback($array[$i], $array[$j])) {
                $result = $array[$i];
                $array[$i] = $array[$j];
                $array[$j] = $result;
            }
        }
    }

    return $array;
}

// Для двумерных массивов :
// Транспонировать матрицу :
function transpose($array)
{
    $transpose = [];

    foreach ($array as $strVal => $arr) {
        foreach ($arr as $key => $value) {
            $transpose[$key][$strVal] = $value;
        }
    }

    return $transpose;
}

// Сложить две матрицы :
function matrixSum($matrixA, $matrixB)
{
    $matrixSum = [];

    for ($i = 0; $i < count($matrixA); $i++) {
        for ($j = 0; $j < count($matrixB[$i]); $j++) {
            $matrixSum[$i][$j] = $matrixA[$i][$j] + $matrixB[$i][$j];
        }
    }

    return $matrixSum;
}

// Удалить те строки, в которых сумма элементов положительна и присутствует хотя бы один нулевой элемент:
function deleteRow($matrixA) 
{
    foreach ($matrixA as $strVal => $row) {
        $isZero = false;
        $sum = 0;

        foreach($row as $value) {
            $sum += $value;
            if ($value === 0) {
                $isZero = true;
            }
        }
        
        if ($isZero && $sum > 0) {
            unset($matrixA[$strVal]);
        }
    }

    return array_values($matrixA);
}

// Удалить те столбцы, в которых сумма элементов положительна и присутствует хотя бы один нулевой элемент:
function deleteColumn($matrixA)
{
    $sizeMatrix = count($matrixA);
    $duplicateMatrix = $matrixA;
    $sizeDuplicate = count($duplicateMatrix);

    for ($i = 0; $i < $sizeMatrix; $i++) {
        for ($j = 0; $j < $sizeMatrix; $j++) {
            $sum = 0;
            $isZero = false;
            for ($k = 0; $k < $sizeMatrix; $k++) {
                $sum += $matrixA[$k][$j];
                if ($matrixA[$k][$j] === 0) {
                    $isZero = true;
                }
            }
            if ($sum > 0 && $isZero) {
                for ($n = 0; $n < $sizeDuplicate; $n++) {
                    unset($duplicateMatrix[$n][$j]);
                }
            }
        }
        break;
    }

    return array_map('array_values', $duplicateMatrix);
}

// Рекурсии:
//Написать рекурсивную функцию которая будет обходить и выводить все значения любого массива и любого уровня вложенности :
function arrayRecursive($array)
{
    $values = '';

    foreach ($array as $item) {
        if (is_array($item)) {
            $values .= arrayRecursive($item);
        } else {
            $values .= $item;
        }
    }

    return $values;
}