// Написать функцию которая по параметрам принимает число из десятичной системы счисления и преобразовывает в двоичную.
function decimalBin(decimal)
{
  let binary = '';
  while (decimal >= 1) {
    binary = parseInt(decimal % 2) + binary;
    decimal /= 2;
  }
  return binary;
}

// С помощью рекурсии :
function decimalBin(decimal)
{
  let binary = '';
  if (decimal < 1) {
    return 0;
  }
  return binary = parseInt(decimal % 2) + 10 * decimalBin(decimal / 2);
}

// Написать функцию которая по параметрам принимает число из двоичной системы счисления и преобразовывает в десятичную.
function binaryDec(binary)
{
  let decimal = 0;
  let base = 1;
  while (binary) {
    let lastDigit = binary % 10;
    binary = parseInt(binary / 10);
    decimal = decimal + lastDigit * base;
    base = base * 2;
  }
  return decimal;
}

// С помощью рекурсии :
function binaryDec(binary)
{
  let decimal = '';
  if (binary < 1) {
    return 0;
  }
  return decimal = parseInt(binary % 10) + 2 * binaryDec(binary / 10);
}

// Написать функцию которая выводит первые N чисел фибоначчи
function fibonacciNumbers(number)
{
  if (number === 0) {
    return 0;
  }
  if (number === 1 || number === 2) {
    return 1;
  }

  let fibonacci1 = 0;
  let fibonacci2 = 1;
  let counter = 0;
  let countFibonacci = [fibonacci1];
  while (counter < number - 1) {
    let fibonacciSum = fibonacci1 + fibonacci2;
    fibonacci1 = fibonacci2;
    fibonacci2 = fibonacciSum;
    countFibonacci.push(fibonacci1);
    counter++;
  }
  return countFibonacci;
}

// Через рекурсию :
function fibonacciNumbers(number)
{
  if (number <= 1) {
    return 0;
  }
  if (number === 2) {
    return 1;
  }
  return fibonacciNumbers(number - 1) + fibonacciNumbers(number - 2);
}

// Написать функцию, возведения числа N в степень M
function power(number, degree)
{
  let result = 1;
  for (let i = 1; i <= degree; i++) {
    result *= number;
  }
  return result;
}Title

// Через рекурсию :
function power(number, degree)
{
  if (degree === 1) {
    return number;
  }
  if (degree !== 1) {
    return number * power(number, degree - 1);
  }
}

// Написать функцию которая вычисляет входит ли IP-адрес в диапазон указанных IP-адресов. Вычислить для версии ipv4.
function ipRange(startIp, endIp, ip)
{
  startIp = startIp.split('.');
  endIp = endIp.split('.');
  ip = ip.split('.');
  let ipArrays = [startIp, endIp, ip];
  for (let ipArray of ipArrays) {
    if (ipArray.length !== 4) {
      return false;
    } else {
      for (let number of ipArray) {
        number = parseInt(number);
        if (!(number >= 0 && number <= 255 && Number.isInteger(number))) {
          return false;
        }
      }
    }
  }
  for (let i = 0; i < ip.length; i++) {
    if (!(ip[i] >= startIp[i] && ip[i] <= endIp[i])) {
      return false;
    }
  }
  return true;
}

// Для одномерного массива : подсчитать процентное соотношение положительных/отрицательных/нулевых/простых чисел
// 1.1 процентное соотношение положительных чисел :
let isPositiveNum = function isPositiveNum(value) {
  return value > 0;
}

// 1.2 процентное соотношение отрицательных чисел :
let isNegativeNum = function isNegativeNum(value) {
  return value < 0;
}

// 1.3 процентное соотношение нулевых чисел :
let isZeroNum = function isZeroNum(value) {
  return value === 0;
}

// 1.4 процентное соотношение простых чисел :
let isPrimeNum = function isPrimeNum(value) {
  if (value <= 1) {
    return false;
  }
  for (let i = 2; i < value; i++) {
    if (value % i === 0) {
      return false;
    }
  }
  return true;
}

// Через рекурсию :
let isPrimeNum = function isPrimeNum(value, divider = 0) {
  if (divider === 0) {
    divider = value - 1;
  }
  if (value <= 1) {
    return false;
  }
  while (divider >= 2) {
    if (value % divider === 0) {
      return false;
    }
    return isPrimeNum(value, divider - 1);
  }
  return true;
}

function getPercentage(array, callback)
{
  let count = array.length;
  let percent = 0;
  let counter = 0;
  for (value of array) {
    if (callback(value)) {
      counter++;
    }
  }
  return counter * 100 / count;
}

// 1.5 Отсортировать элементы по возрастанию/убыванию:
// по возрастанию:
let sortAsc = function sortAsc(arrayValue1, arrayValue2) {
  return arrayValue1 < arrayValue2;
}

let sortDesc = function sortDesc(arrayValue1, arrayValue2) {
  return arrayValue1 > arrayValue2;
}

function sortVal(array, callback)
{
  let count = array.length;
  let result = 0;
  for (let i = count - 1; i > 0; i--) {
    for (j = 0; j < i; j++) {
      if (callback(array[i], array[j])) {
        result = array[i];
        array[i] = array[j];
        array[j] = result;
      }
    }
  }
  return array;
}

// Для двумерных массивов :
// Транспонировать матрицу :
function transpose(array)
{
  let transposeArr = [];
  for (let i = 0; i < array[0].length; i++) {
    transposeArr[i] = [];

    for (let j = 0; j < array.length; j++) {
      transposeArr[i][j] = array[j][i];
    }
  }
  return transposeArr;
}

// Сложить две матрицы :
function matrixSum(matrixA, matrixB)
{
  let matrixSum = [];
  for (let i = 0; i < matrixA.length; i++) {
    matrixSum[i] = [];
    for (let j = 0; j < matrixB[i].length; j++) {
      matrixSum[i][j] = matrixA[i][j] + matrixB[i][j];
    }
  }
  return matrixSum;
}

// Удалить те строки, в которых сумма элементов положительна и присутствует хотя бы один нулевой элемент:
function deleteRow(matrixA)
{
  matrixA = matrixA.filter(function (row) {
    let isZero = false;
    let sum = 0;
    row.map(function (value) {
      sum += value;
      if (value === 0) {
        isZero = true;
      }
    });
    return !(isZero && sum > 0);
  });
  return matrixA;
}

// Удалить те столбцы, в которых сумма элементов положительна и присутствует хотя бы один нулевой элемент:
function deleteColumn(matrix)
{
  let sizeMatrix = matrix.length;
  for (let i = 0; i < sizeMatrix; i++) {
    for (let j = 0; j < sizeMatrix; j++) {
      let sum = 0;
      let isZero = false;
      for (let k = 0; k < sizeMatrix; k++) {
        sum += matrix[k][j];
        if (matrix[k][j] === 0) {
          isZero = true;
        }
      }
      if (sum > 0 && isZero) {
        for (let n = 0; n < sizeMatrix; n++) {
          matrix[n].splice(j, 1);
        }
      }
    }
  }
  return matrix;
}

// Рекурсии:
//Написать рекурсивную функцию которая будет обходить и выводить все значения любого массива и любого уровня вложенности :
function arrayRecursive(array)
{
  let values = '';
  for (let item of array) {
    if (typeof item === 'object') {
      values += arrayRecursive(item);
    } else {
      values += item;
    }
  }
  return values;
}
