<?php

function generateCode($length = 6)
{
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHI JKLMNOPRQSTUVWXYZ0123456789";
    $code = "";
    $clen = strlen($chars) - 1;
    while (strlen($code) < $length) {
        $code .= $chars[mt_rand(0, $clen)];
    }
    return $code;
}

require_once "config.php";

if (isset($_POST['submit'])) {
    $userData = $pdo->query("SELECT `user_id`, `user_password` FROM `user` WHERE `user_login` = '".$_POST['login']. "'LIMIT 1")->fetchAll(PDO::FETCH_ASSOC);
    if ($userData[0]['user_password'] === md5(md5($_POST['password']))) {
        $hash = md5(generateCode(10));
        $query = $pdo->prepare( "UPDATE `user` SET `user_hash` = ? WHERE `user_id` = ?");
        $query->execute([$hash, $userData[0]['user_id']]);
        setcookie("id", $userData[0]['user_id'], time() + 60 * 60 * 24 * 30, "/");
        setcookie("hash", $hash, time() + 60 * 60 * 24 * 30, "/", null, null, true);
        header("Location: check.php");
        exit();
    } else {
        print "Вы ввели неправильный логин/пароль";
    }
}
?>

<form method="POST">
    Логин <input name="login" type="text" required><br>
    Пароль <input name="password" type="password" required><br>
    <input name="submit" type="submit" value="Войти">
</form>