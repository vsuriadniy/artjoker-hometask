<?php

include_once "config.php";

$allowedResult = [$_GET['result']];
$allUser = $pdo->query("SELECT * FROM `user` WHERE `user_id` = '" . $_COOKIE['id'] . "';")->fetchAll(PDO::FETCH_ASSOC);
$array = $pdo->query("SELECT `user_login` FROM `user` WHERE `user_id` = '" . $_COOKIE['id'] . "';")->fetchAll(PDO::FETCH_ASSOC);
$user = $array[0]['user_login'];

if (in_array($_GET['result'], $allowedResult)) {
    $filed = '';
    if ($_GET['result'] === 'bot') {
        $field = 'bot_victories';
    }
    if ($_GET['result'] === 'draw') {
        $field = 'draw';
    }
    if ($_GET['result'] === $user) {
        $field = 'victories';
    }
    $statement = $pdo->prepare("UPDATE `user` SET $field = $field + 1 WHERE `user_id` = ? ;");
    $status = $statement->execute([$_COOKIE['id']]);
    $result['data'] = $pdo->query("SELECT `user_login`, `victories`, `bot_victories`, `draw` FROM `user` WHERE `user_id` = '" . $_COOKIE['id'] . "';")->fetchAll(PDO::FETCH_ASSOC);
    $result['status'] = $status;
    $message = 'Victories : ';
    foreach ($result['data'] as $value) {
        $message .= $value['user_login'] . ': ' . $value['victories'] . ', ' . 'bot: ' . $value['bot_victories'] . ', ' . ' draw: ' . $value['draw'] . ', ';
    }
    $message = mb_substr($message, 0, -2);
    $result['message'] = $message;
    echo json_encode($result);
    die();
}