<?php
include_once "config.php";
$query = $pdo->query("SELECT `user_login` FROM `user` WHERE user_id = '" . $_COOKIE['id'] . "';")->fetchAll(PDO::FETCH_ASSOC);
$userLogin = $query[0]['user_login'];
print_r($userLogin);
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Игра</title>
    <link rel="stylesheet" href="main.css">
</head>
<body>
    <a href="/logout.php">Выйти</a>
    <h1>Крестики-нолики</h1>
    <div class="result"></div>
    <div class="area-wrapper">
        <div id="area">
            <div></div>
            <div class="box"></div>
            <div class="box"></div>
            <div class="box"></div>
            <div class="box"></div>
            <div class="box"></div>
            <div class="box"></div>
            <div class="box"></div>
            <div class="box"></div>
            <div class="box"></div>
        </div>
    </div>
    <div id="modal-result-wrapper">
        <div id="overlay"></div>
        <div id="modal-window">
            <div id="content"></div>
            <div id="list_winner"></div>
            <div id="btn-close">Play again</div>
        </div>
    </div>
    <script>
      let register_user = '<?php echo $userLogin; ?>';
    </script>
    <script src="main.js"></script>
</body>
</html>