<?php

require_once "config.php";

if (isset($_POST['submit'])) {
    $errors = [];
    if (!preg_match("/^[a-zA-Z0-9]+$/", $_POST['login'])) {
        $errors[] = "Логин может состоять только из букв английского алфавита и цифр";
    }

    if (strlen($_POST['login']) < 3 || strlen($_POST['login']) > 30) {
        $errors[] = "Логин должен быть не меньше 3-х символов и не больше 30";
    }

    $query = $pdo->prepare("SELECT COUNT(`user_id`) FROM `user` WHERE `user_login` = ?");
    $query->execute([$_POST['login']]);
    $rowCount = $query->fetchColumn();
    if ($rowCount > 0) {
        $errors[] = "Пользователь с таким логином уже существует в базе данных";
    }

    if (count($errors) === 0) {
        $login = $_POST['login'];
        $password = md5(md5(trim($_POST['password'])));
        $query = $pdo->prepare("INSERT INTO `user` SET `user_login` = ?, `user_password` = ?");
        $query->execute([$login, $password]);
        header("Location: authorization.php");
        exit();
    } else {
        print "<b>При регистрации произошли следующие ошибки:</b><br>";
        foreach ($errors as $error) {
            print $error . "<br>";
        }
    }
}
