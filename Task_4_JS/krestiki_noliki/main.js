const area = document.getElementById('area');
const listWinner = document.getElementById('list_winner');
const contentWrapper = document.getElementById('content');
const modalResult = document.getElementById('modal-result-wrapper');
const overlay = document.getElementById('overlay');
const btnClose = document.getElementById('btn-close');
const boxes = document.getElementsByClassName('box');
const sizeBoxes = boxes.length;
const winCombinations = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6]
];
const sizeArr = winCombinations.length;
let result = '';

area.addEventListener('click', e => {
  let box = e.target;
  if (box.innerHTML === '') {
    box.innerHTML = 'X';
    if (emptyFields().length > 0) {
      checkMoves();
    }
    checkWin();
  }
});

function checkMoves()
{
  if (emptyFields().length > 0) {
    for (let i = 0; i < sizeArr; i++) {
      if (boxes[winCombinations[i][0]].innerHTML === '0' && boxes[winCombinations[i][1]].innerHTML === '0' && boxes[winCombinations[i][2]].innerHTML === '') {
        boxes[winCombinations[i][2]].innerHTML = '0';
        return false;
      }
      if (boxes[winCombinations[i][1]].innerHTML === '0' && boxes[winCombinations[i][2]].innerHTML === '0' && boxes[winCombinations[i][0]].innerHTML === '') {
        boxes[winCombinations[i][0]].innerHTML = '0';
        return false;
      }
      if (boxes[winCombinations[i][0]].innerHTML === '0' && boxes[winCombinations[i][2]].innerHTML === '0' && boxes[winCombinations[i][1]].innerHTML === '') {
        boxes[winCombinations[i][1]].innerHTML = '0';
        return false;
      }
    }
    for (let i = 0; i < sizeArr; i++) {
      if (boxes[winCombinations[i][0]].innerHTML === 'X' && boxes[winCombinations[i][1]].innerHTML === 'X' && boxes[winCombinations[i][2]].innerHTML === '') {
        boxes[winCombinations[i][2]].innerHTML = '0';
        return false;
      }
      if (boxes[winCombinations[i][1]].innerHTML === 'X' && boxes[winCombinations[i][2]].innerHTML === 'X' && boxes[winCombinations[i][0]].innerHTML === '') {
        boxes[winCombinations[i][0]].innerHTML = '0';
        return false;
      }
      if (boxes[winCombinations[i][0]].innerHTML === 'X' && boxes[winCombinations[i][2]].innerHTML === 'X' && boxes[winCombinations[i][1]].innerHTML === '') {
        boxes[winCombinations[i][1]].innerHTML = '0';
        return false;
      }
    }
    let emptyArr = emptyFields();
    let random = emptyArr[Math.floor(Math.random() * emptyArr.length)];
    boxes[random].innerHTML = '0';
    return false;
  }
}

function emptyFields()
{
  let emptyArr = [];
  for (let i = 0; i < sizeBoxes; i++) {
    if (boxes[i].innerHTML === '') {
      emptyArr.push(i);
    }
  }
  return emptyArr;
}

function checkWin()
{
  for (let i = 0; i < sizeArr; i++) {
    if (boxes[winCombinations[i][0]].innerHTML === 'X' && boxes[winCombinations[i][1]].innerHTML === 'X' && boxes[winCombinations[i][2]].innerHTML === 'X') {
      result = register_user;
      endGame(result);
      break;
    } else if (boxes[winCombinations[i][0]].innerHTML === '0' && boxes[winCombinations[i][1]].innerHTML === '0' && boxes[winCombinations[i][2]].innerHTML === '0') {
      result = 'bot';
      endGame(result);
      break;
    } else if (emptyFields().length === 0) {
      result = 'draw';
      endGame(result);
      break;
    }
  }
}

function endGame(winner)
{
  fetch(window.location.origin + '/send.php?result=' + winner)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      if (data.status) {
        if (winner === register_user || winner === 'bot') {
          contentWrapper.innerHTML = `${winner} win !`;
          listWinner.innerHTML = data.message;
        } else {
          contentWrapper.innerHTML = `Fighting ${winner} !`;
          listWinner.innerHTML = data.message;
        }
        modalResult.style.display = 'block';
      }
    });
}

function reloadPage()
{
  modalResult.style.display = 'none';
  location.reload();
}

overlay.addEventListener('click', reloadPage);
btnClose.addEventListener('click', reloadPage);