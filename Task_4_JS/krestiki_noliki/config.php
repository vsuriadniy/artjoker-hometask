<?php

$config = include_once "DbConfig.php";
$dsn = "mysql:host=" . $config["host"] . ";dbname=" . $config["dbname"] . ";charset=" . $config["charset"];
$pdo = new PDO($dsn, $config["username"], $config["password"]);