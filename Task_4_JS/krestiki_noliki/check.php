<?php

require_once "config.php";

if (isset($_COOKIE['id']) && isset($_COOKIE['hash'])) {
    $userData = $pdo->query( "SELECT * FROM `user` WHERE `user_id` = '".$_COOKIE['id']."' LIMIT 1")->fetchAll(PDO::FETCH_ASSOC);
    if (($userData[0]['user_hash'] !== $_COOKIE['hash']) || ($userData[0]['user_id'] !== $_COOKIE['id'])) {
        setcookie("id", "", time() - 3600 * 24 * 30 * 12, "/");
        setcookie("hash", "", time() - 3600 * 24 * 30 * 12, "/", null, null, true);
        print "Хм, что-то не получилось";
    } else {
        print "Привет, " . $userData[0]['user_login'] . ". Всё работает!";
        header("Location: index.php");
    }
} else {
    print "Включите куки";
}
?>

<form method="POST">
    Логин <input name="login" type="text" required><br>
    Пароль <input name="password" type="password" required><br>
    <input name="submit" type="submit" value="Войти">
</form>