1. Посчитать сколько пользователей живет в каждой стране и каждом городе.

Для города :
SELECT COUNT(*) AS `Total`, `city`.`title` AS `City` FROM `user` INNER JOIN `city` ON (`user`.`city_id` = `city`.`id`) GROUP BY `city`.`id`;

Для страны :
SELECT COUNT(*) AS `Total`, `country`.`title` AS `Country` FROM `user` INNER
JOIN `city` ON (`user`.`city_id` = `city`.`id`) INNER JOIN `country` ON (`city`.`country_id` = `country`.`id`) GROUP BY `country`.`id`;


2. Посчитать сколько пользователей живет в каждой стране и в каждом городе которые авторизовались за последние трое суток:

Для стран :
SELECT COUNT(DISTINCT(`user_autorization`.`user_id`)) AS `Total`, `country`.`title` AS `Country` FROM `user` INNER JOIN `city` ON (`user`.`city_id` = `city`.`id`) INNER JOIN `country` ON (`city`.`country_id` = `country`.`id`) INNER JOIN `user_autorization` ON (`user`.`id` = `user_autorization`.`user_id`) WHERE `date` >= NOW() - INTERVAL 3 DAY GROUP BY `country`.`id`;

Для городов :
SELECT COUNT(DISTINCT(`user_autorization`.`user_id`)) AS `Total`, `city`.`title` AS `City` FROM `user` INNER JOIN `city` ON (`user`.`city_id` = `city`.`id`) INNER JOIN `user_autorization` ON (`user`.`id` = `user_autorization`.`user_id`) WHERE `date` >= NOW() - INTERVAL 3 DAY GROUP BY `city`.`id`;

	
3.Найти ту страну и тот город у которых максимальное количество авторизаций за (все время/за последние три дня/за последний месяц/квартал/год).

Для СТРАНЫ ЗА ПОСЛЕДНИЕ ТРИ ДНЯ/МЕСЯЦ/КВАРТАЛ/ГОД с полем `Total`, МЕНЯЕМ ТОЛЬКО ЦИФРУ ПОСЛЕ СЛОВА `INTERVAL` НА 30, 90, 365:
SELECT COUNT(*) AS `Total`, `country`.`title` AS `Country` FROM `user` INNER JOIN `city` ON (`user`.`city_id` = `city`.`id`) INNER JOIN `country` ON (`city`.`country_id` = `country`.`id`) INNER JOIN `user_autorization` ON (`user`.`id` = `user_autorization`.`user_id`) WHERE `date` >= NOW() - INTERVAL 3 DAY GROUP BY `country`.`id` ORDER BY `Total` desc LIMIT 1;

Для СТРАНЫ ЗА ВСЕ ВРЕМЯ с полем `Total`:
SELECT COUNT(*) AS `Total`, `country`.`title` AS `Country` FROM `user` INNER JOIN `city` ON (`user`.`city_id` = `city`.`id`) INNER JOIN `country` ON (`city`.`country_id` = `country`.`id`) INNER JOIN `user_autorization` ON (`user`.`id` = `user_autorization`.`user_id`) GROUP BY `country`.`id` ORDER BY `Total` desc LIMIT 1;

Для ГОРОДА ЗА ПОСЛЕДНИЕ ТРИ ДНЯ/МЕСЯЦ/КВАРТАЛ/ГОД с полем `Total`, МЕНЯЕМ ТОЛЬКО ЦИФРУ ПОСЛЕ СЛОВА `INTERVAL` НА 30, 90, 365:
SELECT COUNT(*) AS `Total`, `city`.`title` AS `City` FROM `user` INNER JOIN `city` ON (`user`.`city_id` = `city`.`id`) INNER JOIN `user_autorization` ON (`user`.`id` = `user_autorization`.`user_id`) WHERE `date` >= NOW() - INTERVAL 3 DAY GROUP BY `city`.`id` ORDER BY `Total` desc LIMIT 1;

Для ГОРОДА ЗА ВСЕ ВРЕМЯ с полем `Total`:
SELECT COUNT(*) AS `Total`, `city`.`title` AS `City` FROM `user` INNER JOIN `city` ON (`user`.`city_id` = `city`.`id`) INNER JOIN `user_autorization` ON (`user`.`id` = `user_autorization`.`user_id`) GROUP BY `city`.`id` ORDER BY `Total` desc LIMIT 1;

Для СТРАНЫ ЗА ПОСЛЕДНИЕ ТРИ ДНЯ/МЕСЯЦ/КВАРТАЛ/ГОД без поля `Total`, МЕНЯЕМ ТОЛЬКО ЦИФРУ ПОСЛЕ СЛОВА `INTERVAL`:
SELECT `Country` FROM (SELECT COUNT(*) AS `Total`, `country`.`title` AS `Country` FROM `user` INNER JOIN `city` ON (`user`.`city_id` =
`city`.`id`) INNER JOIN `country` ON (`city`.`country_id` = `country`.`id`) INNER JOIN `user_autorization` ON (`user`.`id` = `user_autorization`.`user_id`) WHERE `date` >= NOW() - INTERVAL 3 DAY GROUP BY `country`.`id` ORDER BY `Total` desc LIMIT 1) `Result_table`;


4. Вывести список стран по возрастанию количества пользователей в стране. (Аналогично сделать для городов).

Для стран:
SELECT COUNT(*) AS `Total`, `country`.`title` AS `Country` FROM `user` INNER JOIN `city` ON (`user`.`city_id` = `city`.`id`) INNER JOIN `country` ON (`city`.`country_id` = `country`.`id`) GROUP BY `country`.`id` ORDER BY `Total` asc;

Для городов:
SELECT COUNT(*) AS `Total`, `city`.`title` AS `City` FROM `user` INNER JOIN `city` ON (`user`.`city_id` = `city`.`id`)  GROUP BY `city`.`id` ORDER BY `Total` asc;


5. Посчитать сколько проживает пользователей по ролям в каждом городе/каждой стране.

Для стран ЧЕРЕЗ UNION:

SELECT COUNT(*) AS `Total`, `country`.`title` AS `Country`, `role`.`name` AS `Role` FROM `user` INNER JOIN `city` ON (`user`.`city_id` = `city`.`id`) INNER JOIN `country` ON (`city`.`country_id` = `country`.`id`) INNER JOIN `role` ON (`user`.`role_id` = `role`.`id`) WHERE `role`.`name` = 'user' GROUP BY `country`.`id`
UNION
SELECT COUNT(*) AS `Total`, `country`.`title` AS `Country`, `role`.`name` AS `Role` FROM `user` INNER JOIN `city` ON (`user`.`city_id` = `city`.`id`) INNER JOIN `country` ON (`city`.`country_id` = `country`.`id`) INNER JOIN `role` ON (`user`.`role_id` = `role`.`id`) WHERE `role`.`name` = 'admin' GROUP BY `country`.`id`
UNION
SELECT COUNT(*) AS `Total`, `country`.`title` AS
`Country`, `role`.`name` AS `Role` FROM `user` INNER JOIN `city` ON (`user`.`city_id` = `city`.`id`) INNER JOIN `country` ON (`city`.`country_id` = `country`.`id`) INNER JOIN `role` ON (`user`.`role_id` = `role`.`id`) WHERE `role`.`name` = 'guest' GROUP BY `country`.`id` ORDER BY `Total`;

Для городов ЧЕРЕЗ UNION:

SELECT COUNT(*) AS `Total`, `city`.`title` AS `City`, `role`.`name` AS `Role` FROM `user` INNER JOIN `city` ON (`user`.`city_id` = `city`.`id`) INNER JOIN `role` ON (`user`.`role_id` = `role`.`id`) WHERE `role`.`name` = 'admin' GROUP BY `user`.`city_id`
UNION
SELECT COUNT(*) AS `Total`, `city`.`title` AS `City`, `role`.`name` AS `Role` FROM `user` INNER JOIN `city` ON (`user`.`city_id` = `city`.`id`) INNER JOIN `role` ON (`user`.`role_id` = `role`.`id`) WHERE `role`.`name` = 'user' GROUP BY `user`.`city_id`;

ОСНОВНОЕ решение ДЛЯ СТРАН:
SELECT COUNT(`user`.`id`) AS `Total`, `country`.`title` AS `Country`, `role`.`name` AS `Role` FROM `user` INNER JOIN `city` ON (`user`.`city_id` = `city`.`id`) INNER JOIN `country` ON (`city`.`country_id` = `country`.`id`) INNER JOIN `role` ON (`user`.`role_id` = `role`.`id`) GROUP BY `country`.`title`, `role`.`name`;

ОСНОВНОЕ решение ДЛЯ ГОРОДОВ:
SELECT COUNT(*) AS `Total`, `city`.`title` AS `City`, `role`.`name` AS `Role` FROM `user` INNER JOIN `city` ON (`user`.`city_id` = `city`.`id`) INNER JOIN `role` ON (`user`.`role_id` = `role`.`id`) GROUP BY `city`.`title`, `role`.`id`;


6. Посчитать сколько раз заходили пользователи с ролью админ за последний месяц в разрезе каждой страны и города.

Для городов:
SELECT COUNT(*) AS `Total`, `city`.`title` AS `City` FROM `user` INNER JOIN `city` ON (`user`.`city_id` = `city`.`id`) INNER JOIN `user_autorization` ON (`user`.`id` = `user_autorization`.`user_id`) INNER JOIN `role` ON (`user`.`role_id` = `role`.`id`) WHERE `date` >= NOW() - INTERVAL 30 DAY && `role`.`name` = 'admin' GROUP BY `city`.`id`;

Для стран:
SELECT COUNT(*) AS `Total`, `country`.`title` AS `Country` FROM `user` INNER JOIN `city` ON (`user`.`city_id` = `city`.`id`) INNER JOIN `country` ON (`city`.`country_id` = `country`.`id`) INNER JOIN `user_autorization` ON (`user`.`id` = `user_autorization`.`user_id`) INNER JOIN `role` ON (`user`.`role_id` = `role`.`id`) WHERE `date` >= NOW() - INTERVAL 30 DAY && `role`.`name` = 'admin' GROUP BY `country`.`id`;


7. Найти всех пользователей у которых нет ни одной роли.

SELECT `user`.`firstname` AS `Firstname`, `role`.`name` AS `Role` FROM `user` LEFT JOIN `role` ON (`user`.`role_id` = `role`.`id`) WHERE `role`.`name` IS NULL;


8. Посчитать сколько проживает в каждой стране и в каждом городе пользователей по всем существующим ролям.

SELECT COUNT(`user`.`id`) AS `Total`, `country`.`title` AS `Country`, `role`.`name` AS `Role` FROM `user` INNER JOIN `city` ON (`user`.`city_id` = `city`.`id`) INNER JOIN `country` ON (`city`.`country_id` = `country`.`id`) RIGHT JOIN `role` ON (`user`.`role_id` = `role`.`id`) GROUP BY `country`.`title`, `role`.`name`;
