<?php

namespace controllers;

use system\View;
use models\News;

class HomeController
{
    public function actionMain()
    {
        View::render('main');
    }

    public function actionNews()
    {
        $model = new News();
        $data = $model->getAll();
        View::render('news', ['data' => $data]);
    }
}