<?php

use autoloader\ClassLoader;
use system\Db;
use routes\Route;

try {
    require_once("autoloader/ClassLoader.php");
    ClassLoader::getInstance()->init();
    Db::getConnect();
    Route::run();
} catch (Exception $e) {
    echo $e->getMessage();
}