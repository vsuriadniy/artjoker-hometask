<?php

namespace routes;

class Route
{
    public static function run()
    {
        $path = $_SERVER['REQUEST_URI'];
        $routes = explode('/', $path);
        $controller = $routes[1];
        $action = $routes[2];
        $controller = 'controllers\\' . ucfirst($controller) . 'Controller';
        $action = 'action' . ucfirst($action);

        if (!class_exists($controller)) {
            throw new \ErrorException('Controller does not exist');
        }
        $objController = new $controller;

        if (!method_exists($objController, $action)) {
            throw new \ErrorException('Action does not exist');
        }
        $objController->$action();
    }
}