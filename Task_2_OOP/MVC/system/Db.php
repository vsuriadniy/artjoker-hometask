<?php

namespace system;

use PDO;

class Db
{
    static $obj;

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    private function __construct()
    {
        $config = require_once __DIR__ . "/../configs/DbConfig.php";
        $dsn = "mysql:host=" . $config["host"] . ";dbname=" . $config["dbname"] . ";charset=" . $config["charset"];
        $this->con = new PDO($dsn, $config["username"], $config["password"]);
    }

    static public function getConnect()
    {
        if (empty(self::$obj)) {
            self::$obj = new self();
        }
        return self::$obj->con;
    }

    static public function select($fields, $table, $type = 'ARRAY', $where = '', $limit = '')
    {
        return self::$obj->con->query('SELECT ' . $fields . ' FROM ' . $table)->fetchAll();
    }
}