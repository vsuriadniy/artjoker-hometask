<?php

namespace models;

use system\Db;

class BaseModel
{
    public static $table;

    public static function getAll()
    {
        return Db::select('*', static::$table);
    }
}