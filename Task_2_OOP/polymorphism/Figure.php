<?php

abstract class Figure 
{
    abstract function square();
    abstract function perimeter();
}
