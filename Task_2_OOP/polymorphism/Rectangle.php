<?php

include("Figure.php");

class Rectangle extends Figure
{
	private $width;
	private $height;
	
	public function __construct($width, $height){
		$this->width = $width;
		$this->height = $height;
	}

	function square(): int 
	{
		return $this->width * $this->height;
	}

	function perimeter(): int 
	{
		return 2 * ($this->width + $this->height);
	}
}
