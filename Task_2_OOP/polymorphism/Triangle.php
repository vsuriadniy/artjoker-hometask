<?php

include("Figure.php");

class Triangle extends Figure 
{
	private $sideA;
	private $sideB;
	private $sideC;
	private $height;
	
	public function __construct($sideA, $sideB, $sideC, $height){
		$this->sideA = $sideA;
		$this->sideB = $sideB;
		$this->sideC = $sideC;
		$this->height = $height;
	}

	function square(): int 
	{
		return 0.5 * $this->sideA * $this->height;
	}

	public function perimeter(): int 
	{
		return $this->sideA + $this->sideB + $this->sideC;
	}
}
