<?php

include("Figure.php");

class Circle extends Figure 
{
	private $radius;
	
	public function __construct($radius){
		$this->radius = $radius;
	}

	function square(): int 
	{
		return M_PI * $this->radius * $this->radius;
	}

	function perimeter(): int 
	{
		return 2 * M_PI * $this->radius;
	}
}
