<?php

// В заданиях из первого домашнего задания, где необходима обработка и проверка на корректность входных данных, обработать с помощью исключительных ситуаций.

class DataValidationException extends Exception {}

// Написать функцию которая по параметрам принимает число из десятичной системы счисления и преобразовывает в двоичную.
function decimalBin(int $decimal, int $numberSystem): int
{
    $binary = '';

    if ($decimal < 0) {
        throw new DataValidationException("Number cannot be less than 0 ! ");
    }

    if ($numberSystem !== 2) {
        throw new DataValidationException("Number is not converted to binary ");
    }

    if ($decimal < 1) {
        return 0;
    }

    while ($decimal >= 1) { 
        $binary = $decimal % $numberSystem . $binary; 
        $decimal /= $numberSystem; 
    }

    return $binary;
}

try {
    echo decimalBin(2, 2);
} catch (DataValidationException $e) {
    echo 'Error: ' . $e->getMessage() . "\n";
}


// С помощью рекурсии :
function decimalBin(int $decimal, int $numSystem, int $origNumSystem): int
{
    $binary = '';

    if ($decimal < 0) {
        throw new DataValidationException("Number cannot be less than 0 ! ");
    }

    if ($numSystem !== 2 || $origNumSystem !== 10) {
        throw new DataValidationException("Number is not converted to binary ");
    }

    if ($decimal < 1) {
        return 0;
    }

    return $binary = $decimal % $numSystem + $origNumSystem * decimalBin($decimal / $numSystem, $numSystem, $origNumSystem);
}

try {
    echo decimalBin(5, 2, 10);
} catch (DataValidationException $e) {
    echo 'Error: ' . $e->getMessage();
}


// Написать функцию которая по параметрам принимает число из двоичной системы счисления и преобразовывает в десятичную.
function binaryDec(int $binary, int $numSystem): int
{
    $decimal = 0;
    $base = 1;

    if ($numSystem !== 10) {
        throw new DataValidationException("Number is not converted to decimal ");
    }

    while ($binary) {
        $lastDigit = $binary % $numSystem;
        $binary = $binary / $numSystem;
        $decimal += $lastDigit * $base;
        $base = $base * 2;
    }

    return abs($decimal);
}

try {
    echo binaryDec(1001, 10);
} catch (DataValidationException $e) {
    echo 'Error: ' . $e->getMessage() . "\n";
}

// С помощью рекурсии :
function binaryDec(int $binary, int $numSystem): int
{
    $decimal = '';

    if ($numSystem !== 10) {
        throw new DataValidationException("Number is not converted to decimal ");
    }

    if($binary < 1){
        return 0;
    }

    return $decimal = $binary % $numSystem + 2 * binaryDec($binary / $numSystem, $numSystem);
}

try {
    echo binaryDec(100, 10);
} catch (DataValidationException $e) {
    echo 'Error: ' . $e->getMessage() . "\n";
}

// Написать функцию которая выводит первые N чисел фибоначчи
function fibonacciNumbers(int $number): array
{
    if ($number < 1) {
        throw new DataValidationException("Number cannot be negative ! ");
    }

    if ($number === 0) {
        return 0;
    }

    $fibonacci1 = 0;
    $fibonacci2 = 1;
    $counter = 0;
    $countFibonacci = [$fibonacci1];
    while ($counter < $number - 1) {
        $fibonacciSum = $fibonacci1 + $fibonacci2;
        $fibonacci1 = $fibonacci2;
        $fibonacci2 = $fibonacciSum;
        $countFibonacci[] = $fibonacci1;
        $counter++;
    }
    
    return $countFibonacci;
}

try {
    print_r(fibonacciNumbers(2));
} catch (DataValidationException $e) {
    echo 'Error: ' . $e->getMessage() . "\n";
}

// Через рекурсию :
function fibonacciNumbers(int $number): int
{
    $fibonacciNum = '';

    if ($number <= 1) {
        return 0;
    } 

    if ($number === 2) {
        return 1;
    }

    return $fibonacciNum .= fibonacciNumbers($number - 1) + fibonacciNumbers($number - 2);
}

try {
    print_r(fibonacciNumbers(2));
} catch (DataValidationException $e) {
    echo 'Error: ' . $e->getMessage() . "\n";
}

// Написать функцию, возведения числа N в степень M
function power(int $number, int $degree): int 
{
    $result = 1;

    for ($i = 1; $i <= $degree; $i++){
        $result *= $number;
    }

    return $result;
}

// Через рекурсию :
function power(int $number, int $degree): int
{
    if ($degree === 1){
        return $number;
    }

    if ($degree !== 1){
        return $number * power($number, $degree - 1); 
    }
}

// Написать функцию которая вычисляет входит ли IP-адрес в диапазон указанных IP-адресов. Вычислить для версии ipv4.
function ipRange(string $startIp, string $endIp, string $ip): bool  
{
    $startIp = explode('.', $startIp);
    $endIp = explode('.', $endIp);
    $ip = explode('.', $ip);   
    $ipArray = [$startIp, $endIp, $ip];

    foreach ($ipArray as $ip) {
        if (count($ip) !== 4) {
            return false;    
        } else {
            foreach ($ip as $number) {
                if (!($number >= 0 && $number <= 255 && is_numeric($number))) {
                    return false;    
                }
            }
        }
    }

    for ($i = 0; $i < count($ip); $i++) { 
        if(!($ip[$i] >= $startIp[$i] && $ip[$i] <= $endIp[$i])) {
            return false;
        }  
    }

    return true;
}

// Для одномерного массива : подсчитать процентное соотношение положительных/отрицательных/нулевых/простых чисел
// 1.1 процентное соотношение положительных чисел :
function isPositiveNum(int $value): bool
{
    return $value > 0;
}

// 1.2 процентное соотношение отрицательных чисел :
function isNegativeNum(int $value): bool
{
    return $value < 0;
}

// 1.3 процентное соотношение нулевых чисел :
function isZeroNum(int $value): bool
{
    return $value === 0;
}

// 1.4 процентное соотношение простых чисел :
function isPrimeNum(int $value): bool
{
    if ($value <= 1) {
        return false;
    }

    for ($i = 2; $i < $value; $i++) {
        if ($value % $i === 0) {
            return false;
        }
    }

    return true;
}

// Через рекурсию :
function isPrimeNum(int $value, int $divider = 0): bool 
{
    if ($divider === 0) {
        $divider = $value - 1;
    }

    if ($value <= 1) {
        return false;
    }

    while ($divider >= 2) {
        if ($value % $divider === 0) {
            return false;
        }

        return isPrimeNum($value, $divider - 1);
    }

    return true;
}

function getPercentage(array $array, string $callback): int 
{
    $count = count($array);
    $counter = 0;

    foreach($array as $value){
        if (!is_numeric($value)) {
            throw new DataValidationException("This element must be a number ! ");
        }
        if($callback($value)){
            $counter++; 
        } 
    }

    return $counter * 100 / $count;
}

try {
    var_dump(getPercentage([5,6,11,8], 'isPostiveNum'));
} catch (DataValidationException $e) {
    echo 'Error: ' . $e->getMessage() . "\n";
}

// 1.5 Отсортировать элементы по возрастанию/убыванию:
// по возрастанию:
function sortAsc($arrayValue1, $arrayValue2): bool
{
    return $arrayValue1 < $arrayValue2;
}

function sortDesc($arrayValue1, $arrayValue2): bool
{
    return $arrayValue1 > $arrayValue2;
}

function sortVal(array $array, string $callback): array
{
    foreach ($array as $value) {
        if (!is_string($value) && !is_numeric($value)) {
            throw new DataValidationException("The data received in the array does not correspond to a number, or string!");
        }
    }
    $count = count($array);
    $result = 0;

    for ($i = $count - 1; $i > 0; $i--) {
        for ($j = 0; $j < $i; $j++) {
            if ($callback($array[$i], $array[$j])) {
                $result = $array[$i];
                $array[$i] = $array[$j];
                $array[$j] = $result;
            }
        }
    }

    return $array;
}

try {
    var_dump(sortVal(['winter', 'black', 'apple'], 'sortAsc'));
} catch (DataValidationException $e) {
    echo 'Error: ' . $e->getMessage() . "\n";
}

// Для двумерных массивов :
// Транспонировать матрицу :
function transpose(array $array): array 
{
    $transpose = [];

    foreach ($array as $strVal => $arr) {
        if (!is_array($arr)) {
            throw new DataValidationException("Invalid data type: one of the matrix rows must be of type array ! ");  
        }
        foreach ($arr as $key => $value) {
            if (!is_numeric($value)) {
                throw new DataValidationException("Invalid data type: each element of the matrix must be a number ! "); 
            }
            $transpose[$key][$strVal] = $value;
        }
    }

    return $transpose;
}

try {
    print_r(transpose([
                        [1,2,3], 
                        [1,2,3]
                      ]));
} catch (DataValidationException $e) {
    echo 'Error: ' . $e->getMessage() . "\n";
}

// Сложить две матрицы :
function matrixSum(array $matrixA, array $matrixB): array
{
    $matrixSum = [];

    foreach ($matrixA as $rowMatrixA) {
        if (!is_array($rowMatrixA)) {
            throw new DataValidationException("Invalid data type: one of the matrixA rows must be of type array ! ");  
        }
    }

    foreach ($matrixB as $rowMatrixB) {
        if (!is_array($rowMatrixB)) {
            throw new DataValidationException("Invalid data type: one of the matrixB rows must be of type array ! ");  
        } 
    }

    for ($i = 0; $i < count($matrixA); $i++) {
        for ($j = 0; $j < count($matrixB[$i]); $j++) {
            if (!is_numeric($matrixA[$i][$j])) {
                throw new DataValidationException("Invalid data type: each element of the matrixA must be a number ! ");
            }

            if (!is_numeric($matrixB[$i][$j])) {
                throw new DataValidationException("Invalid data type: each element of the matrixB must be a number ! ");
            }
            $matrixSum[$i][$j] = $matrixA[$i][$j] + $matrixB[$i][$j];
        }
    }

    return $matrixSum;
}

try {
    print_r(matrixSum([
                        [1,2,3], 
                        [1,2,3]
                      ],

                      [
                        [1,2,3],
                        [1,2,3]
                      ]  
                  ));
} catch (DataValidationException $e) {
    echo 'Error: ' . $e->getMessage() . "\n";
}

// Удалить те строки, в которых сумма элементов положительна и присутствует хотя бы один нулевой элемент:
function deleteRow(array $matrixA): array 
{
    $sum = 0;

    foreach ($matrixA as $strVal => $array) {
        if (!is_array($array)) {
            throw new DataValidationException("Invalid data type: one of the matrix rows must be of type array ! ");  
        }
        foreach($array as $key => $value){
            if (!is_numeric($value)) {
                throw new DataValidationException("Invalid data type: each element of the matrix must be a number ! "); 
            }
            $sum += $value;
            if ($value === 0 && $sum > 0){
              unset($matrixA[$strVal]);
            }
        }   
    }

    return array_values($matrixA);
}

try {
    print_r(deleteRow([
                        [1,2,3], 
                        [1,0,3]
                      ]));
} catch (DataValidationException $e) {
    echo 'Error: ' . $e->getMessage();
}

// Удалить те столбцы, в которых сумма элементов положительна и присутствует хотя бы один нулевой элемент:
function deleteColumn(array $matrixA): array
{
    $sizeMatrix = count($matrixA);
    $duplicateMatrix = $matrixA;
    $sizeDuplicate = count($duplicateMatrix);

    foreach ($matrixA as $rowMatrixA) {
        if (!is_array($rowMatrixA)) {
            throw new DataValidationException("Invalid data type: one of the matrixA rows must be of type array ! ");  
        }
    }

    for ($i = 0; $i < $sizeMatrix; $i++) {
        for ($j = 0; $j < $sizeMatrix; $j++) {
            $sum = 0;
            $isZero = false;
            for ($k = 0; $k < $sizeMatrix; $k++) {
                if (!is_numeric($matrixA[$k][$j])) {
                    throw new DataValidationException("Invalid data type: each element of the matrixA must be a number ! ");
                }
                $sum += $matrixA[$k][$j];
                if ($matrixA[$k][$j] === 0) {
                    $isZero = true;
                }
            }
            if ($sum > 0 && $isZero) {
                for ($n = 0; $n < $sizeDuplicate; $n++) {
                    unset($duplicateMatrix[$n][$j]);
                }
            }
        }
        break;
    }

    return array_map('array_values', $duplicateMatrix);
}

try {
    print_r(deleteColumn([
                            [1,2,3], 
                            [1,0,3],
                            [1,2,3]
                         ]));
} catch (DataValidationException $e) {
    echo 'Error: ' . $e->getMessage();
}

// Удалить те столбцы, в которых сумма элементов положительна и присутствует хотя бы один нулевой элемент:
function deleteColumn(array $matrixA): array
{
    $sizeMatrix = count($matrixA);
    $duplicateMatrix = $matrixA;
    $sizeDuplicate = count($duplicateMatrix);

    foreach ($matrixA as $rowMatrixA) {
        if (!is_array($rowMatrixA)) {
            throw new DataValidationException("Invalid data type: one of the matrixA rows must be of type array ! ");  
        }
    }

    for ($i = 0; $i < $sizeMatrix; $i++) {
        for ($j = 0; $j < $sizeMatrix; $j++) {
            $sum = 0;
            $isZero = false;
            for ($k = 0; $k < $sizeMatrix; $k++) {
                if (!is_numeric($matrixA[$k][$j])) {
                    throw new DataValidationException("Invalid data type: each element of the matrixA must be a number ! ");
                }
                $sum += $matrixA[$k][$j];
                if ($matrixA[$k][$j] === 0) {
                    $isZero = true;
                }
            }
            if ($sum > 0 && $isZero) {
                for ($n = 0; $n < $sizeDuplicate; $n++) {
                    unset($duplicateMatrix[$n][$j]);
                }
            }
        }
        break;
    }

    return array_map('array_values', $duplicateMatrix);
}

try {
    print_r(deleteColumn([
                            [1,2,3], 
                            [1,0,3],
                            [1,2,3]
                         ]));
} catch (DataValidationException $e) {
    echo 'Error: ' . $e->getMessage();
}